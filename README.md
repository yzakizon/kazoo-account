# Create Kazoo account


## Create New Kazoo account

This will example below will create new account with name "iam1" with phone number 16502001000.
-a will do get auth token and by default will use master account with superadmin user
-n will create new account
-p will add phone number

```
python3 kazoo.py -a -n  --accountName "iam1" -p --phoneNumber "16502001000"

Output:
Request: PUT  http://localhost:8000/v2/accounts/85fcbbf6c648b9244e91567a3e059304/users
New default admin user created. username="admin@iam1", password="password1234", account_name="iam1", account_id="85fcbbf6c648b9244e91567a3e059304"


Request: PUT  http://localhost:8000/v2/accounts/85fcbbf6c648b9244e91567a3e059304/phone_numbers/16502001000
Phone number added. phoneNumber="16502001000", account_name="iam1", account_id="85fcbbf6c648b9244e91567a3e059304"

```

Admin username will use this format: admin@<accountName>

## Login to account
Your new account will have default admin:
username: admin@iam1
password: password1234
account: iam1
phoneNumber: 16502001000

Now that you created account "iam1", let's login to that accounts

```
python3 kazoo.py -a --username admin@iam1 --password password1234 --authAccountName iam1

Output:
Create kazoo account. Type -h for help
Author: Yeffry Zakizon


Request: PUT  http://localhost:8000/v2/user_auth  .body {'data': {'credentials': '681e1876892f3002d6c1d1a0a56e2ee909ab6665', 'method': 'sha', 'account_name': 'iam1'}}
Auth success. username="admin@iam1" account_name="iam1", account_id="85fcbbf6c648b9244e91567a3e059304"
AUTH_TOKEN="eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImM0ZThjMTA3ZGQ4NzU1YTI3M2Y0YTdmZTM2MTk2ZGI4In0.eyJpc3MiOiJrYXpvbyIsImlkZW50aXR5X3NpZyI6IjhDSHUwRUFhSC10RGZKMDBRV2FEX0VvV3Z1aGNiYzdxLWVWaWxuVFJBY1EiLCJhY2NvdW50X2lkIjoiODVmY2JiZjZjNjQ4YjkyNDRlOTE1NjdhM2UwNTkzMDQiLCJvd25lcl9pZCI6IjMxY2NjNjYxNjE0OWM3NzRhYWQ2YTgyZjM3ZmMwZDA5IiwibWV0aG9kIjoiY2JfdXNlcl9hdXRoIiwiZXhwIjoxNjA1MzI0MjI0fQ.HrzU0c0LKDxfmmNxEJQU1WE5Jyorfkd0XEp7slhNQmw1Q1ZASfZ5aGltdwjBtUKxvr2OuAFJ5vTPMuscNgG7sWvqHZDws8XrpoLAEZEzK9_3OwOUdwBhdkgUV3SFyTfkYlil2Il8OraB9h1TAmBEDlh6T6nsPzBbhgRpa8aNTh-zgexD_GaLgSxUNVqbrj-XEh5GgAPjilcv2qkY0sDw02vrJGJ83TuZ7qqxGsFRjREU72UHhlnyX_0POpP8zZv3EClyvexP4_indva2TyQRglx1EApo-Wo1LBzgZDUYfNRwaUW1DZmUaAYoMes3BdJ5oVUxX-lVFfe6txk6XRU8jA"
```

## Delete account

To delete account use superadmin authentication.

```
python3 kazoo.py -a -d --accountName iam1

Output:
...

Can't find accountName in list. Please run Auth too
```

We don't keep track of accountName and accountId, but accountId is required to delete account. To fix this command use accountId instead.

```
python3 kazoo.py -a -d --accountName 85fcbbf6c648b9244e91567a3e059304

Output:
...

Request: DELETE  http://localhost:8000/v2/accounts/85fcbbf6c648b9244e91567a3e059304
Delete account success, account_name="85fcbbf6c648b9244e91567a3e059304", account_id="85fcbbf6c648b9244e91567a3e059304"

```
