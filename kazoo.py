import requests
import os
import json
import sys
import argparse
import hashlib

DEFAULT_PWD = "password1234"

class Kazoo:
    def __init__(self):
        self.credentials = ""
        self.authToken = ""
        self.url = ""
        self.accounts = {}

    def auth(self, username, password, accountName, phoneNumber):
        # initialize a string
        str = username+":"+password
        # encode the string
        encoded_str = str.encode()
        # create a sha1 hash object initialized with the encoded string
        hash_obj = hashlib.sha1(encoded_str)
        # convert the hash object to a hexadecimal value
        credentials = hash_obj.hexdigest()

        jsonBody = ""
        headers = {}
        body = { "data" : { "credentials" : credentials, "method": "sha"}}

        if phoneNumber is not None and len(phoneNumber) != 0:
            body["data"]["phone_number"] = phoneNumber
        elif accountName is not None and len(accountName) != 0:
            body["data"]["account_name"] = accountName
        else:
            print("Auth requies authAccountName or authPhoneNumber")
            return ""

        url = self.url+'/v2/user_auth'

        print("Request: PUT ", url, " .body", body)
        try:
        	res = requests.put(url, data = json.dumps(body), headers = headers)
        	jsonBody = res.json()
        except Exception as e:
            print("Exception: ", e)
            return ""

        if (res.status_code >= 300):
            print("Auth Error: %d. Response: %s" % (res.status_code, jsonBody))
            return

        accountName  = jsonBody["data"]["account_name"]
        accountId  = jsonBody["data"]["account_id"]
        self.authToken = jsonBody["auth_token"]
        self.accounts[accountName] = accountId

        print("Auth success. username=\"%s\" account_name=\"%s\", account_id=\"%s\"" % (username, accountName, accountId))
        print("AUTH_TOKEN=\""+self.authToken+"\"")
        print("\n")
        return jsonBody

    def newAccount(self, accountName):
        if accountName is None or len(accountName) == 0:
            print("Account name is required")
            return ""

        if len(self.authToken) == 0:
            print("Auth token is required")
            return ""

        jsonBody = ""
        headers = {"X-Auth-Token":  self.authToken}
        body = { "data" : { "name": accountName }}
        url = self.url+'/v2/accounts/'
        print("Request: PUT ", url)

        try:
        	res = requests.put(url, data = json.dumps(body), headers = headers)
        	jsonBody = res.json()
        except Exception as e:
            print("Exception: ", e)
            return ""

        if (res.status_code >= 300):
            print("Create new account Error: %d. Response: %s" % (res.status_code, jsonBody))
            return

        jsonData = jsonBody["data"]
        print("New account response: ", jsonData)
        self.accounts[accountName] = jsonData["id"]
        print("New account created. account_name=\"%s\" account_id=\"%s\"\n" % (accountName, self.accounts[accountName]))
        #print("AUTH_TOKEN=\""+self.AuthToken+"\"")
        print("\n")

        self.createDefaultAdminUser(accountName)

        return jsonBody

    def deleteAccount(self, accountName):
        if accountName is None or len(accountName) == 0:
            print("Account name is required")
            return ""

        if len(self.authToken) == 0:
            print("Auth token is required")
            return ""

        if  len(accountName) < 20 and accountName not in self.accounts:
            print("Can't find accountName in list. Please run Auth")
            return ""

        jsonBody = ""
        headers = {"X-Auth-Token":  self.authToken}
        body = {}

        accountId = accountName
        if (len(accountId) < 20):
            accountId = self.accounts[accountName]

        url = self.url+'/v2/accounts/'+accountId
        print("Request: DELETE ", url)

        try:
        	res = requests.delete(url, data = None, headers = headers)
        	jsonBody = res.json()
        except Exception as e:
            print("Exception: ", e)
            return ""

        if (res.status_code >= 300):
            print("Delete account error %d. Response %s" % (res.status_code, jsonBody))
            return

        print("Delete account success, account_name=\"%s\", account_id=\"%s\"" % (accountName, accountId))
        if accountName in self.accounts:
            del self.accounts[accountName]

        #print("AUTH_TOKEN=\""+self.AuthToken+"\"")
        print("\n")


        return jsonBody

    def createDefaultAdminUser(self, accountName):
        if accountName is None or len(accountName) == 0:
            print("Account name is required")
            return ""

        if len(self.authToken) == 0:
            print("Auth token is required")
            return ""

        if  len(accountName) < 20 and accountName not in self.accounts:
            print("Can't find accountName in list. Please run Auth")
            return ""

        accountId = accountName
        if (len(accountId) < 20):
            accountId = self.accounts[accountName]

        username = "admin@"+accountName
        jsonBody = ""
        headers = {"X-Auth-Token":  self.authToken}
        body = { "data" : { "username": username, "password":DEFAULT_PWD, "first_name":"admin", "last_name": accountName, "priv_level": "admin" }}

        url = self.url+'/v2/accounts/'+accountId+'/users'
        print("Request: PUT ", url)

        try:
        	res = requests.put(url, data = json.dumps(body), headers = headers)
        	jsonBody = res.json()
        except Exception as e:
            print("Exception: ", e)
            return ""

        if (res.status_code >= 300):
            print("Create default admin error: %d %s" % (res.status_code, jsonBody))
            return

        jsonData = jsonBody["data"]
        print("New default admin user created. username=\"%s\", password=\"%s\", account_name=\"%s\", account_id=\"%s\" "
                    % (username, DEFAULT_PWD, accountName, accountId))

        print("\n")
        return jsonBody

    def addPhoneNumber(self, accountName, phoneNumber):
        if len(self.authToken) == 0:
            print("Auth token is required")
            return ""

        if  len(accountName) < 20 and accountName not in self.accounts:
            print("Can't find accountName in list. Please run Auth")
            return ""

        if len(phoneNumber) == 0:
            print("phoneNumber is required")
            return ""

        accountId = accountName
        if (len(accountId) < 20):
            accountId = self.accounts[accountName]

        jsonBody = ""
        headers = {"X-Auth-Token":  self.authToken}
        body = { "data" : { "phone_numb": accountName }}
        url = self.url+'/v2/accounts/'+accountId+'/phone_numbers/'+phoneNumber

        print("Request: PUT ", url)

        try:
        	res = requests.put(url, data = json.dumps(body), headers = headers)
        	jsonBody = res.json()
        except Exception as e:
            print("Exception: ", e)
            return ""

        if (res.status_code >= 300):
            print("Add phone number error: %d %s" % (res.status_code, jsonBody))
            return

        print("Phone number added. phoneNumber=\"%s\", account_name=\"%s\", account_id=\"%s\" "
                    % (phoneNumber, accountName, accountId))
        return jsonBody

if __name__ == '__main__':
    print("Create kazoo account. Type -h for help")
    print("Author: Yeffry Zakizon")
    print("\n")
    parser = argparse.ArgumentParser()
    parser.add_argument("-a","--auth", help="Authenticate to kazoo", action="store_true")
    parser.add_argument("--username", help="Authenticatation username", default="superadmin")
    parser.add_argument("--password", help="Authenticat password", default="somepassword")
    parser.add_argument("--authAccountName", help="Authenticate accountName", default="master")
    parser.add_argument("--authPhoneNumber", help="Authenticate phoneNumber", default="")
    parser.add_argument("--url", help="Kazoo Server URL", default="http://localhost:8000")
    parser.add_argument("-n","--newAccount", help="Create new account", action="store_true")
    parser.add_argument("-d","--delAccount", help="Delete account", action="store_true")
    parser.add_argument("--accountName", help="Account name or account id")
    parser.add_argument("--phoneNumber", help="New phone number ex: 165020010001")
    parser.add_argument("-p","--addPhoneNumber", help="Add phone number", action="store_true")

    args = parser.parse_args()

    kazoo = Kazoo()
    kazoo.url = args.url

    if (args.newAccount and args.delAccount):
        print("Must select create account or delete acount but not both")
        sys.exit()

    if (args.auth):
        kazoo.auth(args.username, args.password, args.authAccountName, args.authPhoneNumber)

    if (args.newAccount):
        kazoo.newAccount(args.accountName)

    if (args.delAccount):
        kazoo.deleteAccount(args.accountName)

    if (args.addPhoneNumber):
        kazoo.addPhoneNumber(args.accountName, args.phoneNumber)
